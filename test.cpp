#define BOOST_TEST_MODULE main
#include <boost/test/included/unit_test.hpp>

#include <algorithm>
#include <iostream>
#include <ranges>
#include <vector>

#include "template_printf.hpp"

BOOST_AUTO_TEST_SUITE(Test)

namespace ranges = std::ranges;
namespace views = std::ranges::views;

char rot13a(const char x, const char a) { return a + (x - a + 13) % 26; }

char rot13(const char x) {
  if ('Z' >= x and x >= 'A')
    return rot13a(x, 'A');

  if ('z' >= x and x >= 'a')
    return rot13a(x, 'a');

  return x;
}

BOOST_AUTO_TEST_CASE(how_to_print_transformed_and_filtered_vector) {
  std::vector<int> v{1, 2, 3, 4, 5};
  ranges::for_each(v | views::transform([](const auto &a) { return a + 1; }) | views::filter([] (const auto& a) { return a%2 == 0;}),  [](const auto &a) { std::cout << a; });
}

BOOST_AUTO_TEST_CASE(how_to_create_a_vector_from_transformed_and_filtered_vector) {
  std::vector<int> v{1, 2, 3, 4, 5};
  auto range = (v | views::transform([](const auto &a) { return a + 1; }) | views::filter([] (const auto& a) { return a%2 == 0;}));
  std::vector<int> transformed(range.begin(), range.end());
  std::vector<int> expected{2,4,6};
  
  BOOST_CHECK(transformed == expected);
}

BOOST_AUTO_TEST_CASE(printf_two_fixed_strings) {
   std::string result = the_printf::template_printf(the_printf::merge_2(the_printf::static_text("AA"), the_printf::static_text("BB")));
   BOOST_CHECK(result == "AABB");
}

BOOST_AUTO_TEST_CASE(printf_two_fixed_strings_and_argument) {
  std::string arg = "XX";
  std::string result = the_printf::template_printf(the_printf::merge_2(the_printf::merge_2(the_printf::static_text("AA"), the_printf::plain_string()), the_printf::static_text("BB")), arg);
  BOOST_CHECK(result == "AAXXBB");
}

BOOST_AUTO_TEST_CASE(printf_two_fixed_strings_and_two_arguments) {
  std::string arg = "XX";
  std::string result = the_printf::template_printf(the_printf::merge_2(the_printf::merge_2(the_printf::merge_2(the_printf::static_text("AA"), the_printf::plain_string()), the_printf::static_text("BB")), the_printf::plain_string()), arg, arg);
  BOOST_CHECK(result == "AAXXBBXX");
}


BOOST_AUTO_TEST_SUITE_END()
