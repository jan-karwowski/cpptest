#ifndef TEMPLATE_PRINTF_H
#define TEMPLATE_PRINTF_H

#include <iostream>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>

namespace the_printf {

template <class Format, class... FormatArgs>
std::string template_printf(const Format &format, const FormatArgs &...args) {
  std::stringstream str;
  format.apply(str, args...);
  return str.str();
}

template <class... Args> struct template_args {
  template <class... Args2>
  template_args<Args..., Args2...> constexpr join(template_args<Args2...>) {
    return template_args();
  }

  template<class... Args2>
  std::pair<std::tuple<Args...>, std::tuple<Args2...>> split_list(Args... args, Args2... args2) {
    return std::make_pair(std::make_tuple(args...), std::make_tuple(args2...));
  }
};

class static_text {
  std::string text;

public:
  typedef template_args<> args;
  static_text(std::string &&text) : text(text) {}
  void inline apply(std::ostream &stream) const { stream << text; }
};

template <class Format1, class Format2>
class merge_2 {
  Format1 format1;
  Format2 format2;

public:
  typedef decltype(typename Format1::args().join(
      typename Format2::args())) args;

  constexpr merge_2(Format1 format1, Format2 format2)
      : format1(format1), format2(format2) {}

  template <class... ArgTypes>
  void inline apply(std::ostream &stream, const ArgTypes&... args) const {

    auto pair = typename Format1::args().split_list(args...);

    std::apply([this, &stream](auto... tuple_args) { format1.apply(stream, tuple_args...);}, pair.first);
    std::apply([this, &stream](auto... tuple_args) { format2.apply(stream, tuple_args...);}, pair.second);
  }

  template <class... ArgTypes>
  requires(std::is_same_v<template_args<>, typename Format1::args>)
  void inline apply(std::ostream &stream, const ArgTypes&... args) const {
    format1.apply(stream);
    format2.apply(stream, args...);
  }

  template <class... ArgTypes>
  requires(std::is_same_v<template_args<>, typename Format2::args> && !std::is_same_v<template_args<>, typename Format1::args>)
  void inline apply(std::ostream &stream, const ArgTypes&... args) const {
    format1.apply(stream, args...);
    format2.apply(stream);
  }

  
};

class plain_string {
public:
  typedef template_args<std::string> args;
  void inline apply(std::ostream &stream, const std::string &arg) const {
    stream << arg;
  }
};

} // namespace the_printf

#endif /* TEMPLATE_PRINTF_H */
