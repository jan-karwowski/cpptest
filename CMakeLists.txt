cmake_minimum_required (VERSION 3.12.0)

project(cpptest)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# add_compile_options(-fsanitize=address,undefined)
# add_link_options(-fsanitize=address,undefined)

find_program(CCACHE_PROGRAM ccache)
if(CCACHE_PROGRAM)
  set(CMAKE_CXX_COMPILER_LAUNCHER "${CCACHE_PROGRAM}")
endif()

include(CTest)

find_package(Boost REQUIRED COMPONENTS unit_test_framework)

add_executable(cpptest test.cpp)
target_link_libraries(cpptest Boost::unit_test_framework)
target_compile_options(cpptest PRIVATE -Wall -Wextra)
add_test(NAME test COMMAND cpptest)
